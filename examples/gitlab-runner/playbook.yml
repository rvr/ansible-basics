---
- hosts: all
  become: true

  vars_files: vars.yml

  tasks:
    - name: Fix arch values for gitlab-runner
      set_fact:
        gitlab_package_arch: "{{ ( ansible_architecture == 'x86_64' ) |
                                ternary('i686',ansible_architecture) }}"

    - name: Set full hostname
      hostname:
        # underscore is not valid character for hostname,
        # it needs to be changed for arch x86_64 
        name: "runner.{{ ansible_architecture|replace('_', '-') }}"

    - name: SELinux - allow gitlab-runner to connect to servers
      # setsebool -P nis_enabled 1
      seboolean:
        name: nis_enabled
        state: true
        persistent: true

    - name: Find the right package manager config file
      find:
        paths: /etc
        file_type: file
        recurse: yes
        depth: 2
        patterns:
          - yum.conf
          - dnf.conf
      register: pkg_conf

    - name: Disable best matching to get docker-ce installed/upgraded
      lineinfile:
        path: "{{ item.path }}"
        regexp: 'best='
        line: "best=False"
        owner: root
        group: root
        mode: '0644'
      loop: "{{ pkg_conf.files }}"

    - name: Ensure all packages are updated
      package:
        name: "*"
        state: latest

    - name: Ensure some debugging tools are installed
      package:
        name:
          - bash-completion
          - tmux
          - vim
        state: present

    - name: Ensure conflicting packages are removed
      package:
        name: "{{ conflicting_packages }}"
        state: absent

    - name: Install docker
      include_tasks: "tasks/docker-{{ ansible_architecture }}.yml"

    - name: Install gitlab-runner
      package:
        name: "https://gitlab.com/cki-project/gitlab-runner/-/jobs/760607820/artifacts/raw/out/rpm/gitlab-runner_{{ gitlab_package_arch }}.rpm"
        state: present
        # Disble until we can use the upstream packages
        disable_gpg_check: yes
      notify: "Enable and start gitlab-runner"

    - name: Save private key for worker access
      copy:
        # ssh won't accept the key without the final line-feed 🤦
        content: "{{ lookup('env', 'GITLAB_RUNNER_SSH_PRIVATE_KEY') }}\n"
        dest: "/etc/gitlab-runner/worker-key"
        owner: root
        group: root
        mode: '0600'

    - name: Save public key for worker access
      copy:
        content: "{{ lookup('env', 'GITLAB_RUNNER_AUTOMATION_SSH_PUBLIC_KEY') }}"
        dest: "/etc/gitlab-runner/worker-key.pub"
        owner: root
        group: root
        mode: '0600'

  handlers:

    - name: Enable and start gitlab-runner
      systemd:
        name: gitlab-runner
        enabled: true
        state: started

    - name: Enable and start docker socket
      systemd:
        name: docker.socket
        enabled: true
        state: started

    - name: Enable and start docker service
      systemd:
        name: docker.service
        enabled: true
        state: started
