#!/bin/bash

set -euo pipefail

# Variable definitions
packages=(sudo git htop curl wget vim tmux)
user_name="jon"
user_full_name="Jon Snow"
user_group="jon"
user_groups="${user_group},users"
user_uid=1001
user_gid=1001
sudoers_file="/etc/sudoers.d/$user_name"

if [ "$(id -u)" != 0 ]; then
  echo "You need to be root or use 'sudo' for using this sctip"
  exit 1
fi


# Install dependencies
echo "* Installing packages..."
dnf install -y "${packages[@]}"

# Create group
echo -ne "* Adding the group ${user_group}...\t"
if grep -q "$user_name" /etc/group ; then
  echo "The group ${user_group} already exist. Noting to do." 
else
  groupadd -g "$user_gid" "$user_name"
  echo "Ok"
fi

# Create user
echo -ne "* Adding the user account...\t"
if grep -q "$user_name" /etc/passwd ; then
  echo "The user ${user_name} already exist. Noting to do." 
else
  adduser -c "$user_full_name" -g "$user_gid" -u "$user_uid" \
          -G "$user_groups" "$user_name"
  echo "Ok"
fi

# Add user to sudoers
echo -ne "* Adding the user ${user_name} to the sudoers...\t"
if [ -f "$sudoers_file" ]; then
  echo "The file ${sudoers_file} already exist. Nothing to do."
else
  echo "${user_name} ALL = (ALL:ALL) NOPASSWD: ALL" > "$sudoers_file"
  chmod 0440 "$sudoers_file"
  echo "Ok"
fi

# Check if the new sudoers file is correct
echo -ne "* Checking the sudores file is correct...\t"
visudo -cf "$sudoers_file"
