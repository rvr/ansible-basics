# The way of the automation

## Then the script

```bash
#!/bin/bash

# Install dependencies
sudo dnf install -y git htop curl wget vim tmux

# Create group
sudo groupadd -g 1001 jon

# Create user
sudo adduser -c "Jon Snow" -g 1001 -u 1001 -G jon,users jon

# Add user to sudoers
echo "jon ALL = (ALL:ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers.d/jon
sudo chmod 0440 /etc/sudoers.d/jon
```
