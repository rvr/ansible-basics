# The way of the automation

## Add more users

```bash
#!/bin/bash

set -euo pipefail

# Variable definitions
packages=(sudo git htop curl wget vim tmux)
declare -A user_1
user_1=(
  [name]="jon"
  [full_name]="Jon Snow"
  [group]="jon"
  [groups]="${user_1[group]},users"
  [uid]=1001
  [gid]=1001
)
declare -A user_2
user_2=(
  [name]="arya"
  [full_name]="Arya Stark"
  [group]="arya"
  [groups]="${user_2[group]},users"
  [uid]=1002
  [gid]=1002
)

if [ "$(id -u)" != 0 ]; then
  echo "You need to be root or use 'sudo' for using this sctip"
  exit 1
fi


# Install dependencies
echo "* Installing packages..."
dnf install -y "${packages[@]}"

for n in 1 2; do
  # Some bash black magick
  declare -n user="user_$n"

  # Create group
  echo -ne "* Adding the group ${user[group]}...\t"
  if grep -q "${user[name]}" /etc/group ; then
    echo "The group ${user[group]} already exist. Noting to do." 
  else
    groupadd -g "${user[gid]}" "${user[name]}"
    echo "Ok"
  fi

  # Create user
  echo -ne "* Adding the user account...\t"
  if grep -q "${user[name]}" /etc/passwd ; then
    echo "The user ${user[name]} already exist. Noting to do." 
  else
    adduser -c "${user[full_name]}" -g "${user[gid]}" -u "${user[uid]}" \
            -G "${user[groups]}" "${user[name]}"
    echo "Ok"
  fi

  # Add user to sudoers
  sudoers_file="/etc/sudoers.d/${user[name]}"
  echo -ne "* Adding the user ${user[name]} to the sudoers...\t"
  if [ -f "$sudoers_file" ]; then
    echo "The file ${sudoers_file} already exist. Nothing to do."
  else
    echo "${user[name]} ALL = (ALL:ALL) NOPASSWD: ALL" > "$sudoers_file"
    chmod 0440 "$sudoers_file"
    echo "Ok"
  fi

  # Check if the new sudoers file is correct
  echo -ne "* Checking the sudores file is correct...\t"
  visudo -cf "$sudoers_file"
done
```
