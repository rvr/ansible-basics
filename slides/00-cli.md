# The way of the automation

## At the begining it was the command line

### Install dependencies

```shell
sudo dnf install -y git htop curl wget vim tmux
```

### Create the user's group

```shell
sudo groupadd -g 1001 jon
```

### Create the user's account

```shell
sudo adduser -c "Jon Snow" -g 1001 -u 1001 -G jon,users jon
```

### Add the user to the sudo (without password)

```shell
echo "jon ALL = (ALL:ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers.d/jon
sudo chmod 0440 /etc/sudoers.d/jon
```
